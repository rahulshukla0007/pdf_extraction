from fastapi import FastAPI
from typing import List
import concurrent
import uvicorn
from difflib import SequenceMatcher
from PyPDF2 import PdfFileReader
from tabula import read_pdf
import numpy as np
from tabula import convert_into
import pandas as pd
import time
import re
import os
import glob
import multiprocessing as mp
import pyodbc
from datetime import date
import sys
import subprocess
import shutil
import asyncio
from typing import List
import secrets
from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi import File, UploadFile,BackgroundTasks
from fastapi.responses import HTMLResponse
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

security = HTTPBasic()


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


def get_current_username(credentials: HTTPBasicCredentials = Depends(security)):
    correct_username = secrets.compare_digest(credentials.username, "bchydrouser")
    correct_password = secrets.compare_digest(credentials.password, "bchydro@1234")
    if not (correct_username and correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    return credentials.username

fileName = ""
table_code = ""
table_code_list = []
document_name = ""
start_page = 0
end_page = 0
skipPages = []
table_name = ""
final_columns = 15
csvPath = ''
excelPath = ''
service_state =[]
manager = mp.Manager()
logging_list = manager.list()


@app.get("/initialize/")
def initialize():
    global csvPath,excelPath,fileName, table_code, table_code_list,document_name,start_page,end_page,skipPages,table_name,final_columns
    os.chdir(r"/home/bchydrouser/FastAPI_env/pdf_files")
    for file in glob.glob("*.pdf"):
        print(file)
        split_name = file.split(".pdf")
        fileName = split_name[0]
        table_code_list = re.findall("ID+\d{4}|CD+\d{3}|CFC+\d{3}",fileName)
        Submission_date = re.findall("\d{4}-\d{2}-\d{2}",fileName)
        
        table_code = table_code_list[0]

    pdf = PdfFileReader(fileName+".pdf","rb")
    start_page      = 10
    end_page        = pdf.getNumPages()

    document_name   = table_code +"_"+ Submission_date[0]
    os.mkdir("/home/bchydrouser/FastAPI_env/output_files/"+table_code)
    path            = "/home/bchydrouser/FastAPI_env/output_files/"+table_code
    os.mkdir(path+"/Initial_Csvs/")
    os.mkdir(path+"/Output_Csvs/")
    csvPath         = path + "/Initial_Csvs/test{}.csv"
    excelPath       = path + "/Output_Csvs/BCH_Page{}.dat"
    skipPages       = list()
    table_name      = "BCH_" + table_code + "_20201217"
    final_columns   = 15

def pdf_process():
    initialize()
    workerpool()
    datamerge_insert()
 #print(logging_list)
    print("Process Completed")
    logging_list.append("Process Completed")


@app.post("/uploadfiles/")    
def create_upload_files(background_tasks: BackgroundTasks , files: UploadFile = File(...)):
    os.chdir(r"/home/bchydrouser/FastAPI_env/pdf_files")   
    with open(files.filename, "wb") as buffer:
        shutil.copyfileobj(files.file, buffer)
    buffer.close()
    background_tasks.add_task(pdf_process )
    return {"File Upload Completed"}
#    await initialize()
#    await workerpool()
#    await datamerge_insert()
    
  #  return {"filenames: [file.filename for file in files]"}


@app.get("/")
def main(username: str = Depends(get_current_username)):
#async def main():
    content = """
<body>
</form>
<form action="/uploadfiles/" enctype="multipart/form-data" method="post">
<input name="files" type="file" multiple>
<input type="submit">
</form>
</body>
    """
    return HTMLResponse(content=content)

@app.get("/logs")
def logs():
    global logging_list,service_state
    string1 = ""
    print(service_state)
    string1 = "\n".join(logging_list)
    string2 = "\n".join(service_state)
    print(service_state)
    string = string1 + string2
    return string








#@app.get("/test")
def workerpool():
    global logging_list
    time_start = time.time()
    logging_list.append(time.strftime('%m/%d/%Y %H:%M:%S'))
    print(time.strftime('%m/%d/%Y %H:%M:%S'))
    logging_list.append("Start Time:"+str(time_start))
    print("Start Time:", time_start)

    try:
        with concurrent.futures.ProcessPoolExecutor(max_workers=(mp.cpu_count()-1)) as executor:
            res = executor.map(extract_text, range(start_page,end_page))
    except Exception as e:
        logging_list.append(str(e))
        print(e)
        

    time_end = time.time()
    logging_list.append(time.strftime('%m/%d/%Y %H:%M:%S'))
    print(time.strftime('%m/%d/%Y %H:%M:%S'))
    print("End Time:", time_end)
    logging_list.append("End Time:"+str(time_end))
    df_skipPages = pd.DataFrame(skipPages, columns=['Page_Number', 'Exception'])
    logging_list.append("Time Required for execution:"+str( time_end - time_start))
    print("Time Required for execution:", time_end - time_start)
    return({"message":"Completed workerpool"})

def extract_text(page_number):
    global logging_list
    os.chdir("/home/bchydrouser/FastAPI_env/pdf_files/")
    logging_list.append("Page Number "+str(page_number)) 
    print('Page Number: ',page_number)
    try:
        full_pdf_df = pd.DataFrame([],
                                   columns=['File_Name','Page_Number','Upload_Date','Document_Name','Cost_Type', 'Resource Type', 'Group', 'Subgroup', 'Description',
                                            'Pay Class',
                                            'ResourceName', 'WBS Name', 'Activity Name', 'Date', 'Record_Values'])
        df1 = pd.DataFrame([], columns=['Resource Type', 'Group', 'Subgroup', 'Description',
                                        'Pay Class',
                                        'ResourceName', 'WBS Name', 'Activity Name'])

        convert_into(fileName+".pdf",csvPath.format(page_number),stream = True,guess = False,output_format = 'csv',pages='{}'.format(page_number))
        df_tableData = pd.read_csv(csvPath.format(page_number),engine='python',skiprows=1)
        df_tableName = pd.read_csv(csvPath.format(page_number),engine='python',nrows=1,header=None)
        
        tablename_TrueFalse_List = list(df_tableName.iloc[0,:].isnull())


        for column in range(len(tablename_TrueFalse_List)):
            if(tablename_TrueFalse_List[column] == False):
                tablename = df_tableName.iloc[0,column]

        Unnamed_TrueFalse_List = df_tableData.columns.str.contains('^Unnamed')

        df_tableData_cols = list(df_tableData.columns)
        match = SequenceMatcher(None, df_tableData_cols[5], 'Resource Name').ratio()
        match1 = SequenceMatcher(None, df_tableData_cols[6], 'Resource Name').ratio()
        match2 = SequenceMatcher(None, df_tableData_cols[4], 'Resource Name').ratio()

        for column in range(len(Unnamed_TrueFalse_List)):
            if ((Unnamed_TrueFalse_List[column] == True) and (~(df_tableData.iloc[:,column].isnull().all())) and (df_tableData.iloc[:,(column-1)].isnull().all())) :
                for data in range(df_tableData.shape[0]):
                     if (pd.notnull(df_tableData.iloc[data,column])):
                        df_tableData.iloc[data,column-1] = df_tableData.iloc[data,column]

        if ((match > .95) | (match1 > .95) | (match2 >.95)):
            Final_df_tableData = df_tableData.loc[:, ~df_tableData.columns.str.contains('^Unnamed')]
            Final_df_tableData = Final_df_tableData.loc[:, ~Final_df_tableData.columns.str.contains('^Grand ')]

        else:
            col = df1.shape[1]
            df1 = df1.append(df_tableData)
            col_8 = re.split((
                ' (Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\s+\d{1,2},\s+\d{4}'),
                df1.columns[col])
            col_9 = re.split((col_8[0] + " "), df1.columns[col])
            df2 = df_tableData[df1.columns[col]].astype(str).str.split(expand=True)

            if (df1.iloc[df1.shape[0] - 1, 0] == 'Grand Total'):
                df2.iloc[df2.shape[0] - 1, 1] = df2.iloc[df2.shape[0] - 1, 0]
                df2.iloc[df2.shape[0] - 1, 0] = np.nan
            if (df2.shape[1] == 2):
                df1['ResourceName'] = df2[0]
                df1.insert(8, col_9[1], df2[1])
                Final_df_tableData = df1.loc[:, ~df1.columns.str.contains('^Unnamed')]
                Final_df_tableData = Final_df_tableData.loc[:, ~Final_df_tableData.columns.str.contains('^Grand ')]
                Final_df_tableData.drop(Final_df_tableData.columns[col + 1], axis=1, inplace=True)
            else:
                df1['ResourceName'] = df2[0]
                df1.rename(columns={df1.columns[col + 1]: col_9[1]}, inplace=True)
                Final_df_tableData = df1.loc[:, ~df1.columns.str.contains('^Unnamed')]
                Final_df_tableData = Final_df_tableData.loc[:, ~Final_df_tableData.columns.str.contains('^Grand ')]
                Final_df_tableData.drop(Final_df_tableData.columns[col], axis=1, inplace=True)

                        


        Final_df_tableData_cols=list(Final_df_tableData.columns)

        non_date_cols=list()

        for i in range(len(Final_df_tableData_cols)):
            x=re.search('(Jan(uary)?|Feb(ruary)?|Mar(ch)?|Apr(il)?|May|Jun(e)?|Jul(y)?|Aug(ust)?|Sep(tember)?|Oct(ober)?|Nov(ember)?|Dec(ember)?)\s+\d{1,2},\s+\d{4}',Final_df_tableData_cols[i])
            if(x==None):
                non_date_cols.append(Final_df_tableData_cols[i])
            
        Final_df_tableData=Final_df_tableData.melt(id_vars=non_date_cols, var_name = 'Date',value_name ='Record_Values')
        
        Final_df_tableData = Final_df_tableData.sort_values(non_date_cols)
        Final_df_tableData.insert(0,"File_Name",fileName)
        Final_df_tableData.insert(1,"Page_Number",page_number)
        Final_df_tableData.insert(2,"Upload_Date",date.today())
        Final_df_tableData.insert(3, 'Document_Name',document_name)
        Final_df_tableData.insert(4,'Cost_Type',tablename)
        
        Final_df_tableData['Record_Values'] = Final_df_tableData['Record_Values'].replace(",","",regex = True)
        Final_df_tableData['Record_Values'] = Final_df_tableData['Record_Values'].replace(" ", "", regex=True)
        Final_df_tableData['Record_Values'] = Final_df_tableData['Record_Values'].replace("-", "", regex=True)
        Final_df_tableData["Record_Values"] = pd.to_numeric(Final_df_tableData["Record_Values"])
        full_pdf_df=full_pdf_df.append(Final_df_tableData)
        df_col_list = list(full_pdf_df.columns)
        if ('Sub-Group' in df_col_list):
            full_pdf_df['Subgroup'] = full_pdf_df['Subgroup'].combine_first(full_pdf_df['Sub-Group'])
            full_pdf_df.drop(['Sub-Group'], axis=1, inplace=True)

        if ('Resource Name' in df_col_list):
            full_pdf_df['ResourceName'] = full_pdf_df['ResourceName'].combine_first(full_pdf_df['Resource Name'])
            full_pdf_df.drop(['Resource Name'], axis=1, inplace=True)
        logging_list.append("Done "+str(page_number))
        print("Done: ",page_number)
        full_pdf_df.to_csv(excelPath.format(page_number),sep = '|', index=False)

    except Exception as e:
        
        skipPages.append([page_number,e])
        print(skipPages)
        logging_list.append(str(skipPages))

#@app.get("/insert")
def datamerge_insert():
    global service_state
    extension = 'dat'
    all_filenames = [i for i in glob.glob("/home/bchydrouser/FastAPI_env/output_files/"+table_code+"/Output_Csvs/*.{}".format(extension))]
    combined_csv_lineter = pd.concat([pd.read_table(f,sep='|') for f in all_filenames ])
    initial_columns=combined_csv_lineter.shape[1]
    difference=initial_columns-final_columns
    if (difference>0):
        for i in range(difference):
            combined_csv_lineter.drop(combined_csv_lineter.columns[initial_columns-(i+1)],axis=1,inplace=True)
    combined_csv_lineter.to_csv( r"combined_dat_"+table_code+".dat",sep='|', index=False)
    

    try:    
   #     s = subprocess.run(["/opt/mssql-tools/bin/bcp Test_Table in combined_dat_CFC309.dat -S 'synpasedw.database.windows.net' -d 'demopool' -U 'demouser' -P 'demo@pass123' -q -c -t'|' -F 2  "],shell = True, stdout = subprocess.PIPE).stdout.decode("utf-8")
       # service_state =s#.read().splitlines()
        #os.system("/opt/mssql-tools/bin/bcp BCHydro_DFR_DataStore in combined_dat_"+table_code+".dat -S 'bchydrodb.database.windows.net' -d 'BCHydroDb' -U 'bchydrouser' -P 'bchydro@1234' -q -c -t'|' -F 2 ")
        os.system("mv *.pdf ../BCH_FILES/09AUG21/PDF_FILES" )
        os.system("mv *.dat ../BCH_FILES/09AUG21/DAT_FILES")
       # os.system("mv "+table_code+"_"+date.today()+ " ../bch_archive")
        #os.system("sudo rm *")
       # shutil.rmtree("/home/bchydrouser/FastAPI_env/output_files/"+table_code)
     #   os.system('mv /home/bchydrouser/FastAPI') 
        print("bcp completed")
        logging_list.append("bcp completed")
    except Exception as e:
        print(e)
        logging_list.append(str(e))


